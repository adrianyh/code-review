//
//  WalletTokenCardTableViewCell.swift
//  LSOne
//
//  Created by Adrian Young-Hoon on 2016-11-09.
//  Copyright � 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit

class WalletTokenCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgBusinessLogo: UIImageView!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblBusinessFullAddress: UILabel!
    @IBOutlet weak var lblTokenDescription: UILabel!
    @IBOutlet weak var lblTokenAmt: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblLastUsed: UILabel!
    @IBOutlet weak var imgToken_1: UIImageView!
    @IBOutlet weak var imgToken_2: UIImageView!
    @IBOutlet weak var imgToken_3: UIImageView!
    @IBOutlet weak var imgToken_4: UIImageView!
    @IBOutlet weak var imgToken_5: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ logoURL:String,_ companyName:String,_ fullAddress:String,_ tokenDescription:String,_ tokenAmt:Int,_ tokenMax:Int,_ expiryDate:String,_ lastUsed:String) {
        print("logoURL: \(logoURL)")
        if logoURL.contains("NoLogo.jpg") {
            imgBusinessLogo.image = LS1.textToImage(drawText: companyName as NSString, inImage: UIImage(named: "nologo")!, atPoint: CGPoint(x: 10, y: 5))
        }
        else {
            loadImage(logoURL, imgLogo: imgBusinessLogo)
        }
        //loadImage(logoURL, imgLogo: imgBusinessLogo)
        lblBusinessName.text = companyName
        lblBusinessFullAddress.text = fullAddress
        lblTokenDescription.text = tokenDescription
        lblTokenAmt.text = String(tokenAmt)+"/"+String(tokenMax)
        lblExpiryDate.text = "Expires - "+expiryDate
        lblLastUsed.text = "Last Used - "+lastUsed
        displayMaxTokens(tokenMax)
        setTokens(tokenAmt, tokenMax)
        
        setNeedsUpdateConstraints()
        layoutIfNeeded()
    }
    
    func loadImage(_ urlString:String, imgLogo:UIImageView) {
        let imgURL: URL = URL(string: urlString)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    imgLogo.image = UIImage(data: data!)
                }
                
                DispatchQueue.main.async(execute: display_image)
            }
            
        })
        
        task.resume()
    }
    
    func displayMaxTokens(_ tokenMax: Int) {
        imgToken_1.isHidden = true
        imgToken_2.isHidden = true
        imgToken_3.isHidden = true
        imgToken_4.isHidden = true
        imgToken_5.isHidden = true
        
        let arrTokens = [imgToken_1, imgToken_2, imgToken_3, imgToken_4, imgToken_5]
        for i in 0 ..< tokenMax {
            arrTokens[i]?.isHidden = false
        }
    }
    
    func setTokens(_ tokenAmt:Int, _ tokenMax:Int) {
        switch tokenAmt {
        case 1:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGrey")
                imgToken_3.image = UIImage(named: "tokenGrey")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
            else {
                imgToken_1.image = UIImage(named: "tokenYellow")
                imgToken_2.image = UIImage(named: "tokenGrey")
                imgToken_3.image = UIImage(named: "tokenGrey")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
        case 2:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGrey")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
            else {
                imgToken_1.image = UIImage(named: "tokenYellow")
                imgToken_2.image = UIImage(named: "tokenYellow")
                imgToken_3.image = UIImage(named: "tokenGrey")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
        case 3:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
            else {
                imgToken_1.image = UIImage(named: "tokenYellow")
                imgToken_2.image = UIImage(named: "tokenYellow")
                imgToken_3.image = UIImage(named: "tokenYellow")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
        case 4:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
            else {
                imgToken_1.image = UIImage(named: "tokenYellow")
                imgToken_2.image = UIImage(named: "tokenYellow")
                imgToken_3.image = UIImage(named: "tokenYellow")
                imgToken_4.image = UIImage(named: "tokenYellow")
                imgToken_5.image = UIImage(named: "tokenGrey")
            }
        case 5:
            imgToken_1.image = UIImage(named: "tokenGreen")
            imgToken_2.image = UIImage(named: "tokenGreen")
            imgToken_3.image = UIImage(named: "tokenGreen")
            imgToken_4.image = UIImage(named: "tokenGreen")
            imgToken_5.image = UIImage(named: "tokenGreen")
        default:
            imgToken_1.image = UIImage(named: "tokenGrey")
            imgToken_2.image = UIImage(named: "tokenGrey")
            imgToken_3.image = UIImage(named: "tokenGrey")
            imgToken_4.image = UIImage(named: "tokenGrey")
            imgToken_5.image = UIImage(named: "tokenGrey")
        }
    }

}
