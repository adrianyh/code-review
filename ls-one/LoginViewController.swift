//
//  LoginViewController.swift
//  LS1
//
//  Created by Adrian Young-Hoon on 2016-03-24.
//  Copyright � 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var topStackViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomStackViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var pswdStackView: UIStackView!
    
    var base64Encoded: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        // Swipe Down to Hide Keyboard
        /*
        let swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(LoginViewController.handleSwipeGestureRecognizer(_:)))
        swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Down
        view.addGestureRecognizer(swipeDownGestureRecognizer)
        */
        // Tap to Hide Keyboard
        self.hideKeyboardWhenTapGesture()
        
        self.txtEmail.keyboardType = UIKeyboardType.emailAddress
        self.txtEmail.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLogin(_ sender: AnyObject) {
        // Validate text fields
        
        if (txtEmail.text!.isEmpty){
            self.lblEmail.textColor = UIColor(red: 0.816, green: 0.008, blue: 0.106, alpha: 1)
            self.lblEmail.text = "EMAIL IS REQUIRED"
            self.txtEmail.becomeFirstResponder()
            //alertMessageBox("Email is required")
            return
        }
        else {
            if lblEmail.text != "EMAIL" {
                self.lblEmail.text = "EMAIL"
                self.lblEmail.textColor = UIColor(red: 0.608, green: 0.608, blue: 0.608, alpha: 1)
            }
        }
        if (txtPassword.text!.isEmpty){
            self.lblPassword.textColor = UIColor(red: 0.816, green: 0.008, blue: 0.106, alpha: 1)
            self.lblPassword.text = "PASSWORD IS REQUIRED"
            self.txtPassword.becomeFirstResponder()
            //alertMessageBox("Password is required")
            return
        }
        else {
            if lblPassword.text != "PASSWORD" {
                self.lblPassword.text = "PASSWORD"
                self.lblPassword.textColor = UIColor(red: 0.608, green: 0.608, blue: 0.608, alpha: 1)
            }
        }
        
        let strPSWD = txtPassword.text
        let utf8str = strPSWD!.data(using: String.Encoding.utf8)
        base64Encoded = (utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!
        
        self.callingGetLS1ClientCheck(txtEmail.text! + "|" + base64Encoded)
        
    }

    func alertMessageBox(_ strMsg:String){
        let alertCtrl = UIAlertController(title: "Alert", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
        let actionOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alertCtrl.addAction(actionOK)
        self.present(alertCtrl, animated: true, completion: nil)
    }
    
    func resetLabelHeaders() {
        self.lblEmail.text = "EMAIL"
        self.lblEmail.textColor = UIColor(red: 0.608, green: 0.608, blue: 0.608, alpha: 1)
        self.lblPassword.text = "PASSWORD"
        self.lblPassword.textColor = UIColor(red: 0.608, green: 0.608, blue: 0.608, alpha: 1)
    }
    
    // Mark: - Move TextField Up or Down With Keyboard
    
    func animateTextField(_ textField: UITextField, up: Bool, withOffset offset: CGFloat) {
        let movementDistance: Int = -Int(offset)
        let movementDuration: Double = 0.4
        let movement: Int = (up ? movementDistance: -movementDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        UIView.commitAnimations()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //self.animateTextField(textField, up: true, withOffset: textField.frame.origin.y / 2)
        if (textField.tag == 100){
            topStackViewConstraint.constant = 50 + -(emailStackView.frame.origin.y / 2)
            bottomStackViewConstraint.constant = 5 + (emailStackView.frame.origin.y / 2)
        }
        if (textField.tag == 101){
            topStackViewConstraint.constant = 50 + -(pswdStackView.frame.origin.y / 2)
            bottomStackViewConstraint.constant = 5 + (pswdStackView.frame.origin.y / 2)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //self.animateTextField(textField, up: false, withOffset: textField.frame.origin.y / 2)
        topStackViewConstraint.constant = 50
        bottomStackViewConstraint.constant = 5
}
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Hide Keyboard
    
    func handleSwipeGestureRecognizer(_ gestureRecognizer: UISwipeGestureRecognizer) {
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
    }
    
    // MARK: LS One API Methods
    func callingGetLS1ClientCheck(_ sDoc: String){
        let strDoc = sDoc.replacingOccurrences(of: " ", with: "+", options: NSString.CompareOptions.caseInsensitive, range: nil)
        if let escapedDoc = strDoc.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            let urlPath = "**LS1_API**"
            print(urlPath)
            
            //Alamofire.request(.GET, urlPath, parameters: nil, encoding: .json)
            Alamofire.request(urlPath, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { Response in
                    let json = JSON(Response.result.value ?? "")
                    let clientExist = json["GetLS1ClientCheckResult"][0]["ClientExist"].intValue
                    if clientExist == 1 {
                        self.resetLabelHeaders()
                        UserDefaults.standard.setValue(self.txtEmail.text, forKey: "userID")
                        UserDefaults.standard.setValue(self.base64Encoded, forKey: "passWD")
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.set(false, forKey: "isDeviceTokenUpdated")
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let tabBarController = storyBoard.instantiateViewController(withIdentifier: "tabBarLSOne") as! UITabBarController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = tabBarController
                    }
                    else if clientExist == 2 {
                        self.lblEmail.textColor = UIColor(red: 0.816, green: 0.008, blue: 0.106, alpha: 1)
                        self.lblEmail.text = "YOU HAVE ENTERED AN INVALID EMAIL"
                        self.txtEmail.becomeFirstResponder()
                    }
                    else if clientExist == 3 {
                        self.lblPassword.textColor = UIColor(red: 0.816, green: 0.008, blue: 0.106, alpha: 1)
                        self.lblPassword.text = "YOU HAVE ENTERED AN INVALID PASSWORD"
                        self.txtPassword.becomeFirstResponder()
                    }
                    else {
                        self.lblEmail.textColor = UIColor(red: 208, green: 2, blue: 27, alpha: 1)
                        self.lblEmail.text = "YOU HAVE ENTERED AN INVALID EMAIL"
                        self.lblPassword.textColor = UIColor(red: 208, green: 2, blue: 27, alpha: 1)
                        self.lblPassword.text = "YOU HAVE ENTERED AN INVALID PASSWORD"
                        self.txtEmail.becomeFirstResponder()
                    }
                    print("Client Exist: \(json["GetLS1ClientCheckResult"][0]["ClientExist"])")
                    print("GetLS1ClientCheck Response : \(Response.result.value)")
                    
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func cancelToLoginViewController(_ segue:UIStoryboardSegue) {
        
    }

}
