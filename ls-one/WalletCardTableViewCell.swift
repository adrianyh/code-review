//
//  WalletCardTableViewCell.swift
//  LSOne
//
//  Created by Adrian Young-Hoon on 2016-11-09.
//  Copyright © 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit

class WalletCardTableViewCell: UITableViewCell {

    @IBOutlet weak var imgBusinessLogo: UIImageView!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblBusinessFullAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ logoURL:String,_ companyName:String,_ fullAddress:String) {
        print("logoURL: \(logoURL)")
        if logoURL.contains("NoLogo.jpg") {
            imgBusinessLogo.image = LS1.textToImage(drawText: companyName as NSString, inImage: UIImage(named: "nologo")!, atPoint: CGPoint(x: 10, y: 5))
        }
        else {
            loadImage(logoURL, imgLogo: imgBusinessLogo)
        }
        //loadImage(logoURL, imgLogo: imgBusinessLogo)
        lblBusinessName.text = companyName
        lblBusinessFullAddress.text = fullAddress
        
        //contentView.backgroundColor = UIColor.lightGray
        //contentView.layer.borderColor =  UIColor.lightGray.cgColor
        //contentView.layer.borderWidth = 1
        
        setNeedsUpdateConstraints()
        layoutIfNeeded()
    }

    func loadImage(_ urlString:String, imgLogo:UIImageView) {
        let imgURL: URL = URL(string: urlString)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    imgLogo.image = UIImage(data: data!)
                }
                
                DispatchQueue.main.async(execute: display_image)
            }
            
        })
        
        task.resume()
    }
}
