//
//  Wallet10TokenCardTableViewCell.swift
//  LSOne
//
//  Created by Adrian Young-Hoon on 2016-11-09.
//  Copyright © 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit

class Wallet10TokenCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgBusinessLogo: UIImageView!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblBusinessFullAddress: UILabel!
    @IBOutlet weak var lblTokenDescription: UILabel!
    @IBOutlet weak var lblTokenAmt: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblLastUsed: UILabel!
    @IBOutlet weak var imgToken_1: UIImageView!
    @IBOutlet weak var imgToken_2: UIImageView!
    @IBOutlet weak var imgToken_3: UIImageView!
    @IBOutlet weak var imgToken_4: UIImageView!
    @IBOutlet weak var imgToken_5: UIImageView!
    @IBOutlet weak var imgToken_6: UIImageView!
    @IBOutlet weak var imgToken_7: UIImageView!
    @IBOutlet weak var imgToken_8: UIImageView!
    @IBOutlet weak var imgToken_9: UIImageView!
    @IBOutlet weak var imgToken_10: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ logoURL:String,_ companyName:String,_ fullAddress:String,_ tokenDescription:String,_ tokenAmt:Int,_ tokenMax:Int,_ expiryDate:String,_ lastUsed:String) {
        print("logoURL: \(logoURL)")
        if logoURL.contains("NoLogo.jpg") {
            imgBusinessLogo.image = LS1.textToImage(drawText: companyName as NSString, inImage: UIImage(named: "nologo")!, atPoint: CGPoint(x: 10, y: 5))
        }
        else {
            loadImage(logoURL, imgLogo: imgBusinessLogo)
        }
        //loadImage(logoURL, imgLogo: imgBusinessLogo)
        lblBusinessName.text = companyName
        lblBusinessFullAddress.text = fullAddress
        lblTokenDescription.text = tokenDescription
        lblTokenAmt.text = String(tokenAmt)+"/"+String(tokenMax)
        lblExpiryDate.text = "Expires - "+expiryDate
        lblLastUsed.text = "Last Used - "+lastUsed
        displayMaxTokens(tokenMax)
        setTokens(tokenAmt, tokenMax)
        
        setNeedsUpdateConstraints()
        layoutIfNeeded()
    }
    
    func loadImage(_ urlString:String, imgLogo:UIImageView) {
        let imgURL: URL = URL(string: urlString)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    imgLogo.image = UIImage(data: data!)
                }
                
                DispatchQueue.main.async(execute: display_image)
            }
            
        })
        
        task.resume()
    }
    
    func displayMaxTokens(_ tokenMax: Int) {
        imgToken_1.isHidden = true
        imgToken_2.isHidden = true
        imgToken_3.isHidden = true
        imgToken_4.isHidden = true
        imgToken_5.isHidden = true
        imgToken_6.isHidden = true
        imgToken_7.isHidden = true
        imgToken_8.isHidden = true
        imgToken_9.isHidden = true
        imgToken_10.isHidden = true
        
        if tokenMax == 6 {
            // 234 789 1 5 6 10
            let arrTokens = [imgToken_2, imgToken_3, imgToken_4, imgToken_7, imgToken_8, imgToken_9, imgToken_1, imgToken_5, imgToken_6, imgToken_10]
            for i in 0 ..< tokenMax {
                arrTokens[i]?.isHidden = false
            }
        }
        else if tokenMax == 7 {
            // 1234 789 5 6 10
            let arrTokens = [imgToken_1, imgToken_2, imgToken_3, imgToken_4, imgToken_7, imgToken_8, imgToken_9, imgToken_5, imgToken_6, imgToken_10]
            for i in 0 ..< tokenMax {
                arrTokens[i]?.isHidden = false
            }
            //imgToken_7.frame.size.width = 50
            //imgToken_8.frame.size.width = 50
            //imgToken_9.frame.size.width = 50
            //imgToken_6.isHidden = false
            //imgToken_6.frame.size.width = 25
            //imgToken_10.isHidden = false
            //imgToken_10.frame.size.width = 25
        }
        else if tokenMax == 8 {
            let arrTokens = [imgToken_1, imgToken_2, imgToken_3, imgToken_4, imgToken_6, imgToken_7, imgToken_8, imgToken_9, imgToken_5, imgToken_10]
            for i in 0 ..< tokenMax {
                arrTokens[i]?.isHidden = false
            }
        }
        else {
            let arrTokens = [imgToken_1, imgToken_2, imgToken_3, imgToken_4, imgToken_5, imgToken_6, imgToken_7, imgToken_8, imgToken_9, imgToken_10]
            for i in 0 ..< tokenMax {
                arrTokens[i]?.isHidden = false
            }
        }
    }
    
    func setTokens(_ tokenAmt:Int, _ tokenMax:Int) {
        switch tokenAmt {
        case 1:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGrey")
                imgToken_3.image = UIImage(named: "tokenGrey")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
                imgToken_6.image = UIImage(named: "tokenGrey")
                imgToken_7.image = UIImage(named: "tokenGrey")
                imgToken_8.image = UIImage(named: "tokenGrey")
                imgToken_9.image = UIImage(named: "tokenGrey")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
            else {
                if tokenMax == 6 {
                    imgToken_1.image = UIImage(named: "nologo")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 7 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenGrey")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenGrey")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenGrey")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "tokenGrey")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 2:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGrey")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
                imgToken_6.image = UIImage(named: "tokenGrey")
                imgToken_7.image = UIImage(named: "tokenGrey")
                imgToken_8.image = UIImage(named: "tokenGrey")
                imgToken_9.image = UIImage(named: "tokenGrey")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
            else {
                if tokenMax == 6 {
                    imgToken_1.image = UIImage(named: "nologo")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 7 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenGrey")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "tokenGrey")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 3:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGrey")
                imgToken_5.image = UIImage(named: "tokenGrey")
                imgToken_6.image = UIImage(named: "tokenGrey")
                imgToken_7.image = UIImage(named: "tokenGrey")
                imgToken_8.image = UIImage(named: "tokenGrey")
                imgToken_9.image = UIImage(named: "tokenGrey")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
            else {
                if tokenMax == 6 {
                    imgToken_1.image = UIImage(named: "nologo")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 7 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenGrey")
                    imgToken_5.image = UIImage(named: "tokenGrey")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 4:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "tokenGrey")
                imgToken_6.image = UIImage(named: "tokenGrey")
                imgToken_7.image = UIImage(named: "tokenGrey")
                imgToken_8.image = UIImage(named: "tokenGrey")
                imgToken_9.image = UIImage(named: "tokenGrey")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
            else {
                if tokenMax == 6 {
                    imgToken_1.image = UIImage(named: "nologo")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 7 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "tokenGrey")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 5:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "tokenGreen")
                imgToken_6.image = UIImage(named: "tokenGrey")
                imgToken_7.image = UIImage(named: "tokenGrey")
                imgToken_8.image = UIImage(named: "tokenGrey")
                imgToken_9.image = UIImage(named: "tokenGrey")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
            else {
                if tokenMax == 6 {
                    imgToken_1.image = UIImage(named: "nologo")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenYellow")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 7 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenYellow")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "tokenYellow")
                    imgToken_6.image = UIImage(named: "tokenGrey")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 6:
            // 234 789 1 5 6 10
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "nologo")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "nologo")
                imgToken_6.image = UIImage(named: "nologo")
                imgToken_7.image = UIImage(named: "tokenGreen")
                imgToken_8.image = UIImage(named: "tokenGreen")
                imgToken_9.image = UIImage(named: "tokenGreen")
                imgToken_10.image = UIImage(named: "nologo")
            }
            else {
                if tokenMax == 7 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "nologo")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenYellow")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenYellow")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "tokenYellow")
                    imgToken_6.image = UIImage(named: "tokenYellow")
                    imgToken_7.image = UIImage(named: "tokenGrey")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 7:
            // 4 and 3
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "nologo")
                imgToken_6.image = UIImage(named: "nologo")
                imgToken_7.image = UIImage(named: "tokenGreen")
                imgToken_8.image = UIImage(named: "tokenGreen")
                imgToken_9.image = UIImage(named: "tokenGreen")
                imgToken_10.image = UIImage(named: "nologo")
            }
            else {
                if tokenMax == 8 {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "nologo")
                    imgToken_6.image = UIImage(named: "tokenYellow")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenYellow")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "nologo")
                }
                else {
                    imgToken_1.image = UIImage(named: "tokenYellow")
                    imgToken_2.image = UIImage(named: "tokenYellow")
                    imgToken_3.image = UIImage(named: "tokenYellow")
                    imgToken_4.image = UIImage(named: "tokenYellow")
                    imgToken_5.image = UIImage(named: "tokenYellow")
                    imgToken_6.image = UIImage(named: "tokenYellow")
                    imgToken_7.image = UIImage(named: "tokenYellow")
                    imgToken_8.image = UIImage(named: "tokenGrey")
                    imgToken_9.image = UIImage(named: "tokenGrey")
                    imgToken_10.image = UIImage(named: "tokenGrey")
                }
            }
        case 8:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "nologo")
                imgToken_6.image = UIImage(named: "tokenGreen")
                imgToken_7.image = UIImage(named: "tokenGreen")
                imgToken_8.image = UIImage(named: "tokenGreen")
                imgToken_9.image = UIImage(named: "tokenGreen")
                imgToken_10.image = UIImage(named: "nologo")
            }
            else {
                imgToken_1.image = UIImage(named: "tokenYellow")
                imgToken_2.image = UIImage(named: "tokenYellow")
                imgToken_3.image = UIImage(named: "tokenYellow")
                imgToken_4.image = UIImage(named: "tokenYellow")
                imgToken_5.image = UIImage(named: "tokenYellow")
                imgToken_6.image = UIImage(named: "tokenYellow")
                imgToken_7.image = UIImage(named: "tokenYellow")
                imgToken_8.image = UIImage(named: "tokenYellow")
                imgToken_9.image = UIImage(named: "tokenGrey")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
        case 9:
            if tokenAmt == tokenMax {
                imgToken_1.image = UIImage(named: "tokenGreen")
                imgToken_2.image = UIImage(named: "tokenGreen")
                imgToken_3.image = UIImage(named: "tokenGreen")
                imgToken_4.image = UIImage(named: "tokenGreen")
                imgToken_5.image = UIImage(named: "tokenGreen")
                imgToken_6.image = UIImage(named: "tokenGreen")
                imgToken_7.image = UIImage(named: "tokenGreen")
                imgToken_8.image = UIImage(named: "tokenGreen")
                imgToken_9.image = UIImage(named: "tokenGreen")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
            else {
                imgToken_1.image = UIImage(named: "tokenYellow")
                imgToken_2.image = UIImage(named: "tokenYellow")
                imgToken_3.image = UIImage(named: "tokenYellow")
                imgToken_4.image = UIImage(named: "tokenYellow")
                imgToken_5.image = UIImage(named: "tokenYellow")
                imgToken_6.image = UIImage(named: "tokenYellow")
                imgToken_7.image = UIImage(named: "tokenYellow")
                imgToken_8.image = UIImage(named: "tokenYellow")
                imgToken_9.image = UIImage(named: "tokenYellow")
                imgToken_10.image = UIImage(named: "tokenGrey")
            }
        case 10:
            imgToken_1.image = UIImage(named: "tokenGreen")
            imgToken_2.image = UIImage(named: "tokenGreen")
            imgToken_3.image = UIImage(named: "tokenGreen")
            imgToken_4.image = UIImage(named: "tokenGreen")
            imgToken_5.image = UIImage(named: "tokenGreen")
            imgToken_6.image = UIImage(named: "tokenGreen")
            imgToken_7.image = UIImage(named: "tokenGreen")
            imgToken_8.image = UIImage(named: "tokenGreen")
            imgToken_9.image = UIImage(named: "tokenGreen")
            imgToken_10.image = UIImage(named: "tokenGreen")
        default:
            imgToken_1.image = UIImage(named: "tokenGrey")
            imgToken_2.image = UIImage(named: "tokenGrey")
            imgToken_3.image = UIImage(named: "tokenGrey")
            imgToken_4.image = UIImage(named: "tokenGrey")
            imgToken_5.image = UIImage(named: "tokenGrey")
            imgToken_6.image = UIImage(named: "tokenGrey")
            imgToken_7.image = UIImage(named: "tokenGrey")
            imgToken_8.image = UIImage(named: "tokenGrey")
            imgToken_9.image = UIImage(named: "tokenGrey")
            imgToken_10.image = UIImage(named: "tokenGrey")
        }
    }

}
