//
//  WalletTableViewController.swift
//  LSOne
//
//  Created by Adrian Young-Hoon on 2016-11-08.
//  Copyright © 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications
import AVFoundation

class WalletTableViewController: UITableViewController, GMBLBeaconManagerDelegate {

    var beaconManager: GMBLBeaconManager!
    var myGroup = DispatchGroup()
    var soundNotification: AVAudioPlayer?
    //var errorMessage: String = ""
    var walletCollection = [LS1Wallet]()
    var popUpTapToAddCardViewController = PopUpTapToAddCardViewController(nibName: "PopUpTapToAddCardViewController", bundle: nil)
    var popUpTapToAddCardViewController568 = PopUpTapToAddCardViewController(nibName: "PopUpTapToAddCardViewController568", bundle: nil)
    var popUpTapToAddCardViewController667 = PopUpTapToAddCardViewController(nibName: "PopUpTapToAddCardViewController667", bundle: nil)
    var popUpTapToAddCardViewController736 = PopUpTapToAddCardViewController(nibName: "PopUpTapToAddCardViewController736", bundle: nil)
    var blnDeviceTokenUpdated: Bool = false
    let requestIdentifier = "BeaconMessage" // Identifier is to cancel the notification request
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        Gimbal.setAPIKey("xxxx-xxxx-xxxx-xxxx-xxxx", options: nil)
        
        navigationItem.title = "LS One Wallet"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        /*
        let addCardBarButton = UIBarButtonItem(image: UIImage(named: "addCard"), style: .plain, target: self, action: #selector(addCardToWallet(sender:)))
        //self.navigationItem.rightBarButtonItem?.action
        self.navigationItem.rightBarButtonItem  = addCardBarButton
        */
        tableView.register(UINib(nibName: String(describing: WalletCardTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: WalletCardTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: WalletTokenCardTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: WalletTokenCardTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: Wallet10TokenCardTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: Wallet10TokenCardTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: WalletNoCardsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: WalletNoCardsTableViewCell.self))
        
        tableView.estimatedRowHeight = 275
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self as! UIViewControllerPreviewingDelegate, sourceView: tableView)
        }
        
        if let soundNotification = LS1.setupAudioPlayerWithFile("Birdy_Notification4", type: "wav") {
            self.soundNotification = soundNotification
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        blnDeviceTokenUpdated = UserDefaults.standard.bool(forKey: "isDeviceTokenUpdated")
        let tokenString = UserDefaults.standard.string(forKey: "DeviceToken") ?? ""
        let strUserID = UserDefaults.standard.string(forKey: "userID") ?? ""
        let strPassWD = UserDefaults.standard.string(forKey: "passWD") ?? ""
        if !blnDeviceTokenUpdated && tokenString != "" {
            let objJSON: [String:AnyObject] = ["UserID":strUserID as AnyObject? ?? "" as AnyObject,"PassWD":strPassWD as AnyObject? ?? "" as AnyObject,"DeviceToken":tokenString as AnyObject]
            LS1.callingLS1DeviceTokenUpdate081(objJSON as NSDictionary)
            UserDefaults.standard.set(true, forKey: "isDeviceTokenUpdated")
        }
        
        callingGetLS1Client(strUserID + "|" + strPassWD)
        
        beaconManager = GMBLBeaconManager()
        self.beaconManager.delegate = self
        LS1.startBeaconManager(beaconManager)
    }

    override func viewWillDisappear(_ animated: Bool) {
        LS1.stopBeaconManager(beaconManager)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if walletCollection.count == 0 {
            return 1
        }
        else {
            return walletCollection.count //1
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 //walletCollection.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if walletCollection.count == 0 {
            return 400
        }
        else {
            let myWallet = walletCollection[indexPath.section]
            if myWallet.displayTokens == 1 {
                if myWallet.walletProgressMax > 5 {
                    return 475
                }
                else {
                    return 420
                }
            }
            else {
                return 275
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)

        
        // Configure the cell...
        if walletCollection.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WalletNoCardsTableViewCell.self), for: indexPath) as! WalletNoCardsTableViewCell
            return cell
        }
        else {
            let myWallet = walletCollection[indexPath.section]
            let strLogoName = myWallet.businessLogoURL
            let strBusinessName = myWallet.businessName
            let strBusinessFullAddress = myWallet.businessFullAddress
            let strTokenDescription = myWallet.walletPrize
            let intTokenAmt = myWallet.walletProgress
            let intTokenMax = myWallet.walletProgressMax
            let strExpiryDate = myWallet.walletExpiry
            let strLastUsed = "01/01/1970"
            if myWallet.displayTokens == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WalletCardTableViewCell.self), for: indexPath) as! WalletCardTableViewCell
                print("\(strLogoName), \(strBusinessName), \(strBusinessFullAddress)")
                cell.configure(strLogoName, strBusinessName, strBusinessFullAddress)
                
                let imageCouponView = UIImageView(frame: CGRect(x: cell.frame.width - 45, y: 10, width: 35, height: 35))
                let imageCoupon = UIImage(named: "triangle")
                imageCouponView.image = imageCoupon
                if myWallet.businessCouponActive != 0 {
                    cell.backgroundView = UIView()
                    cell.backgroundView!.addSubview(imageCouponView)
                }
                else {
                    cell.backgroundView?.isHidden = true
                }
                
                // Add border and color
                cell.backgroundColor = UIColor.lightGray
                cell.layer.backgroundColor = UIColor.white.cgColor
                cell.layer.borderColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0).cgColor
                cell.layer.borderWidth = 1
                cell.layer.cornerRadius = 10
                cell.clipsToBounds = true
                return cell
            }
            else {
                if intTokenMax > 5 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: Wallet10TokenCardTableViewCell.self), for: indexPath) as! Wallet10TokenCardTableViewCell
                    cell.configure(strLogoName, strBusinessName, strBusinessFullAddress, strTokenDescription, intTokenAmt, intTokenMax, strExpiryDate, strLastUsed)
                    
                    let imageCouponView = UIImageView(frame: CGRect(x: cell.frame.width - 45, y: 10, width: 35, height: 35))
                    let imageCoupon = UIImage(named: "triangle")
                    imageCouponView.image = imageCoupon
                    if myWallet.businessCouponActive != 0 {
                        cell.backgroundView = UIView()
                        cell.backgroundView!.addSubview(imageCouponView)
                    }
                    else {
                        cell.backgroundView?.isHidden = true
                    }
                    
                    // Add border and color
                    cell.backgroundColor = UIColor.lightGray
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    cell.layer.borderColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0).cgColor
                    cell.layer.borderWidth = 1
                    cell.layer.cornerRadius = 10
                    cell.clipsToBounds = true
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WalletTokenCardTableViewCell.self), for: indexPath) as! WalletTokenCardTableViewCell
                    cell.configure(strLogoName, strBusinessName, strBusinessFullAddress, strTokenDescription, intTokenAmt, intTokenMax, strExpiryDate, strLastUsed)
                    
                    let imageCouponView = UIImageView(frame: CGRect(x: cell.frame.width - 45, y: 10, width: 35, height: 35))
                    let imageCoupon = UIImage(named: "triangle")
                    imageCouponView.image = imageCoupon
                    if myWallet.businessCouponActive != 0 {
                        cell.backgroundView = UIView()
                        cell.backgroundView!.addSubview(imageCouponView)
                    }
                    else {
                        cell.backgroundView?.isHidden = true
                    }
                    
                    // Add border and color
                    cell.backgroundColor = UIColor.lightGray
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    cell.layer.borderColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0).cgColor
                    cell.layer.borderWidth = 1
                    cell.layer.cornerRadius = 10
                    cell.clipsToBounds = true
                    return cell
                }
            }
        }
            
        
        
        //return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sessionDetailsVC = storyboard.instantiateViewController(withIdentifier: String(describing: WalletDetailTableViewController.self)) as! WalletDetailTableViewController
        sessionDetailsVC.passRow = indexPath.section
        navigationController?.pushViewController(sessionDetailsVC, animated: true)

    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnAddCard(_ sender: Any) {
        self.tableView.scrollToTop(navigationBarHeight: (self.navigationController?.navigationBar.frame.size.height)!, animated: true)
        tableView.isScrollEnabled = false
        tabBarController?.setTabBarVisible(visible: false, animated: true)
        navigationController?.isNavigationBarHidden = true
        
        //popUpTapToAddCardViewController.configure(self.view, self, animated: true)
        let screenHeight = UIScreen.main.bounds.size.height
        switch (screenHeight) {
        case 480:
            popUpTapToAddCardViewController.configure(self.view, self, animated: true)
        case 568:
            popUpTapToAddCardViewController568.configure(self.view, self, animated: true)
        case 667:
            popUpTapToAddCardViewController667.configure(self.view, self, animated: true)
        case 736:
            popUpTapToAddCardViewController736.configure(self.view, self, animated: true)
        default:
            popUpTapToAddCardViewController736.configure(self.view, self, animated: true)
        }
    }
    /*
    func addCardToWallet(sender: UIBarButtonItem) {
        self.tableView.scrollToTop(navigationBarHeight: (self.navigationController?.navigationBar.frame.size.height)!, animated: true)
        tableView.isScrollEnabled = false
        tabBarController?.setTabBarVisible(visible: false, animated: true)
        
        popUpTapToAddCardViewController.configure(self.view, self, animated: true)
    }
    */
    func loadImage(_ urlString:String, imgLogo:UIImageView) {
        let imgURL: URL = URL(string: urlString)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    imgLogo.image = UIImage(data: data!)
                }
                
                DispatchQueue.main.async(execute: display_image)
            }
            
        })
        
        task.resume()
    }

    func callingGetLS1Client(_ sDoc: String){
        let strDoc = sDoc.replacingOccurrences(of: " ", with: "+", options: NSString.CompareOptions.caseInsensitive, range: nil)
        if let escapedDoc = strDoc.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            let urlPath = "**LS1_API**"
            print(urlPath)
            
            
            Alamofire.request(urlPath, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { Response in
                    do {
                        print("Response.result.value - \(Response.result.value)")
                        if let jsonResult = try JSONSerialization.jsonObject(with: Response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                            UserDefaults.standard.set(jsonResult, forKey: "myWalletCollection")
                            
                            let json = JSON(Response.result.value ?? "")
                            print("json \(json)")
                            
                            if json["GetLS1ClientResult"].count == 0{
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let loginVC = storyBoard.instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = loginVC
                                
                            } else if json["GetLS1ClientResult"][0]["Wallet"][0]["BID"].stringValue == "" {
                                LS1.loadClientData(jsonResult)
                                DispatchQueue.main.async {
                                    self.tableView.reloadData()
                                }
                                //self.performSegue(withIdentifier: "noCardView", sender: self)
                            } else {
                                DispatchQueue.main.async {
                                    LS1.loadClientData(jsonResult)
                                    if self.walletCollection.count > 0 {
                                        self.walletCollection.removeAll()
                                    }
                                    self.walletCollection = LS1.loadWalletData(jsonResult)
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                    catch {
                        print("Something wrong")
                    }
            }
            
        }
    }
    
    func callingGetLS1BeaconMessage(_ sDoc: String) {
        let strDoc = sDoc.replacingOccurrences(of: " ", with: "+", options: NSString.CompareOptions.caseInsensitive, range: nil)
        if let escapedDoc = strDoc.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            let urlPath = "**LS1_API**"
            print(urlPath)
            
            myGroup.enter()
            Alamofire.request(urlPath, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { Response in
                    UserDefaults.standard.set("", forKey: "notificationMsg")
                    UserDefaults.standard.set("", forKey: "notificationID")
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: Response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                            
                            let json = JSON(jsonResult)
                            if json["GetLS1BeaconMessageResult"].count > 0 {
                                UserDefaults.standard.set(json["GetLS1BeaconMessageResult"][0]["Message"].stringValue, forKey: "notificationMsg")
                                UserDefaults.standard.set(json["GetLS1BeaconMessageResult"][0]["NotificationID"].stringValue, forKey: "notificationID")
                                self.myGroup.leave()
                            }
                            /*
                            else {
                                UserDefaults.standard.set("", forKey: "notificationMsg")
                                UserDefaults.standard.set("", forKey: "notificationID")
                                self.myGroup.leave()
                            }
                            */
                        }
                    }
                    catch {
                        print("Something wrong")
                    }
            }
        }
        myGroup.notify(queue: DispatchQueue.main, execute: {
            print("callingGetLS1BeaconMessage is DONE")
        })
    }
    
    func callingLS1BeaconMessageInsert(_ arrayUser: NSDictionary) -> Bool {
        var blnResult: Bool = false
        let urlPath = "**LS1_API**"
        print(urlPath)
        
        Alamofire.request(urlPath, method: .post, parameters: arrayUser as? [String : AnyObject], encoding: JSONEncoding.default)
            .responseJSON { Response in
                print("Response : \(Response)")
                
                switch Response.result {
                case .success:
                    blnResult = true
                case .failure(let error):
                    blnResult = false
                    //errorMessage = error as! String
                    print("ERROR: \(error)")
                }
        }
        return blnResult
    }
    
    // MARK: GMBLBeaconManager Methods
    func beaconManager(_ manager: GMBLBeaconManager!, didReceive sighting: GMBLBeaconSighting!) {
        let myBeacon = sighting.beacon
        print("Beacon sightings - \(myBeacon?.uuid) RSSI: \(sighting.rssi)")
        
        if sighting.rssi < -70 { //-90
            print("WE HAVE A TAP - RSSI: \(sighting.rssi) \(myBeacon?.uuid)")
            let strUUID = myBeacon?.uuid
            let strCLID = UserDefaults.standard.string(forKey: "CLID")
            LS1.stopBeaconManager(beaconManager)
            
            print("CLID: \(strCLID)")
            print("UUID: \(strUUID)")
            var utf8str = strCLID!.data(using: String.Encoding.utf8)
            let base64EncodedCLID = (utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!
            utf8str = strUUID!.data(using: String.Encoding.utf8)
            let base64EncodedUUID = (utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!
            
            callingGetLS1BeaconMessage(base64EncodedCLID+"|"+base64EncodedUUID)
            let message = UserDefaults.standard.string(forKey: "notificationMsg")
            let messageID = UserDefaults.standard.string(forKey: "notificationID")
            let tokenString = UserDefaults.standard.string(forKey: "DeviceToken") ?? ""
            print("Message: \(message)")
            print("notificationID: \(messageID)")
            if message != nil && message != "" {
                soundNotification?.play()
                
                if #available(iOS 10.0, *) {
                    let content = UNMutableNotificationContent()
                    content.title = "Business Notification"
                    //content.subtitle = ""
                    content.body = message!
                    content.sound = UNNotificationSound.default()
                    
                    // Deliver the notification in five seconds.
                    //let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5.0, repeats: false)
                    let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
                    let request = UNNotificationRequest(identifier:requestIdentifier, content: content, trigger: trigger)
                    
                    UNUserNotificationCenter.current().delegate = self
                    UNUserNotificationCenter.current().add(request){(error) in
                        
                        if (error != nil){
                            
                            print(error?.localizedDescription ?? "Error Unknown")
                        }
                    }
                }
                else {
                    let localNotification = UILocalNotification()
                    localNotification.alertBody = message
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    localNotification.fireDate = Date(timeIntervalSinceNow: 0)
                    localNotification.timeZone = TimeZone.current
                    UIApplication.shared.scheduleLocalNotification(localNotification)
                }
                
                let objJSON: [String:AnyObject] = ["ClientID":strCLID as AnyObject,"BusinessID":strUUID as AnyObject,"NotificationID":messageID as AnyObject,"DeviceToken":tokenString as AnyObject]
                _ = callingLS1BeaconMessageInsert081(objJSON as NSDictionary)
                
            }
            else {
                print("No Message")
                //LS1.startBeaconManager(beaconManager)
            }
        }
    }

}

extension WalletTableViewController:UNUserNotificationCenterDelegate{
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Tapped in notification")
    }
    
    //This is key callback to present notification while the app is in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Notification being triggered")
        //You can either present alert ,sound or increase badge while the app is in foreground too with ios 10
        //to distinguish between notifications
        if notification.request.identifier == requestIdentifier{
            
            completionHandler( [.alert,.sound])
            
        }
    }
}
