//
//  WalletNoCardsTableViewCell.swift
//  LSOne
//
//  Created by Adrian Young-Hoon on 2016-11-15.
//  Copyright © 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit

class WalletNoCardsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
