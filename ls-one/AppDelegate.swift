//
//  AppDelegate.swift
//  LSOne
//
//  Created by Adrian Young-Hoon on 2016-11-08.
//  Copyright © 2016 Adrian Young-Hoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        registerForPushNotifications(application)

        // If user is not logged in then display loginVC screen
        let blnUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        let strUserID = UserDefaults.standard.string(forKey: "userID") ?? ""
        let strPassWD = UserDefaults.standard.string(forKey: "passWD") ?? ""
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyBoard.instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
        
        if (!blnUserLoggedIn) {
            self.window?.rootViewController = loginVC
        }
        else {
            if (strUserID == "" || strPassWD == "") {
                self.window?.rootViewController = loginVC
            }
        }

        // Reset icon badge number
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        UINavigationBar.appearance().titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.purple,
            NSFontAttributeName : UIFont.systemFont(ofSize: 20)
            
        ]
        UINavigationBar.appearance().tintColor = UIColor.purple
        UITabBar.appearance().tintColor = UIColor.purple
        
        return true
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        let aps = userInfo["aps"] as! [String: AnyObject]
        print("aps: \(aps)")
        print("aps alert: \(aps["alert"])")
        
        let json = JSON(userInfo)
        let message = json["aps"]["alert"].stringValue
        //let intBadge = json["aps"]["badge"].intValue
        print("Message: \(message)")
        
        let localNotification = UILocalNotification()
        //var message = ((userInfo.valueForKey("aps") as! String).valueForKey("alert") as! String)
        
        
        localNotification.applicationIconBadgeNumber = 0 //intBadge
        localNotification.alertBody = message
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.fireDate = Date(timeIntervalSinceNow: 0)
        localNotification.timeZone = TimeZone.current
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
            
            //application.registerUserNotificationSettings(notificationSettings)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        print("Device Token NSData: \(deviceToken)")
        print("Device Token: \(tokenString)")
        UserDefaults.standard.setValue(tokenString, forKey: "DeviceToken")
        
        let strUserID = UserDefaults.standard.string(forKey: "userID") ?? ""
        let strPassWD = UserDefaults.standard.string(forKey: "passWD") ?? ""
        print("UserID: \(strUserID)")
        print("PassWD: \(strPassWD)")
        print("UDID: \(UIDevice.current.identifierForVendor!.uuidString)")
        //Update device token
        if strUserID != "" && strPassWD != "" {
            let objJSON: [String:AnyObject] = ["UserID":strUserID as AnyObject? ?? "" as AnyObject,"PassWD":strPassWD as AnyObject? ?? "" as AnyObject,"DeviceToken":tokenString as AnyObject]
            LS1.callingLS1DeviceTokenUpdate081(objJSON as NSDictionary)
        }
        else {
            UserDefaults.standard.set(false, forKey: "isDeviceTokenUpdated")
            //UserDefaults.standard.setValue(tokenString, forKey: "DeviceToken")
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }
    /*
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("Received Local Notification:")
        print(notification.alertBody ?? "No alertBody")
    }
    */
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func registerForPushNotifications(_ application: UIApplication) {
        let viewAction = UIMutableUserNotificationAction()
        viewAction.identifier = "VIEW_IDENTIFIER"
        viewAction.title = "View"
        viewAction.activationMode = .foreground
        
        let newsCategory = UIMutableUserNotificationCategory()
        newsCategory.identifier = "LSONE_NEWS_CATEGORY"
        newsCategory.setActions([viewAction], for: .default)
        
        let categories: Set<UIUserNotificationCategory> = [newsCategory]
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
            
                if granted {
                    
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else {
                    print("ERROR: \(error)")
                }
            })
        }
        else {
            let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: categories)
            application.registerUserNotificationSettings(notificationSettings)
        }
        
        //let notificationSettings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
        //application.registerUserNotificationSettings(notificationSettings)
    }

}

extension UINavigationBar {
    
    var castShadow : String {
        get { return "anything fake" }
        set {
            self.layer.shadowOffset = CGSize(width: 0, height: 3) //CGSizeMake(0, 3)
            self.layer.shadowRadius = 3.0
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOpacity = 0.7
        }
    }
    
}
